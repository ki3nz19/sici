const webpack = require('webpack');

require('dotenv').config();

const { parsed: myEnv } = require('dotenv').config({
    path:'.env.prod'
})

module.exports = {
    compress: true,
    env: {
        API_URL: 'http://localhost:8087/api/v1/',
        BASE_URL: 'http://localhost:3000/',
        META_TITLE: 'Standard Insurance',
        META_DESCRIPTION: 'Standard Insurance offers the most comprehensive car insurance. Get your free quotation now!',
        META_IMAGE: 'https://res.cloudinary.com/standard-insurance/image/upload/v1620324927/sici/Hero-Image-1_vhivfk.png',
        REDUX_PERSIST_KEY: 'ragrc-nclex'
    },
    future: {
      webpack5: true,
    },
    webpack(config) {
        config.plugins.push(new webpack.EnvironmentPlugin(myEnv))
        return config
    },
    poweredByHeader: false,
    images: {
        domains: [
            'localhost',
            'res.cloudinary.com',
        ],
        imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
    }
}