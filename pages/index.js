import Header from '../components/Header'
import NavBar from "../components/NavBar";
import Container from "../components/Container";
import Product from "../components/Home/Product";
import Features from "../components/Home/Features";
import AskQuestion from "../components/Home/AskQuestion";
import StandardNews from "../components/Home/StandarNews";
import PartnersLogo from "../components/Home/PartnersLogo";
import GetAQuote from "../components/Home/GetAQuote";
import Footer from "../components/Footer";
import CarouselBreakPoint from '../components/Home/CarouselBreakPoint';

export default function Home() {
    return (
        <div>
            <Header
                title="Standard Insurance"
            />
            <NavBar />
            <CarouselBreakPoint />
            <Container>
                <GetAQuote />
                <Product />
                <Features />
                <AskQuestion />
                <StandardNews />
                <PartnersLogo />
            </Container>
            <Footer />
        </div>
    )
}
