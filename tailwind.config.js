const defaultTheme = require('tailwindcss/defaultTheme');
const plugin = require('tailwindcss/plugin');

module.exports = {
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundColor: {
        primary: "#278691",
        secondary: "#E8AA33"
      },
      borderColor: {
        primary: "#278691"
      },
      height: {
        lgCarousel: '500px'
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif']
      },
      textColor: {
        primary: "#278691",
        secondary: "#7FDDE7",
        orange: "#E8AA33"
      },
    },
  },
  variants: {
    extend: {
      height: ['hover', 'focus'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require("@heroicons/react/solid"),
    require("@headlessui/react"),
    // Default button
    plugin (function({ addComponents, theme }) {
      const buttons = {
          '.btn': {
            padding: `${theme('spacing.2')} ${theme('spacing.6')}`,
            borderRadius: theme('borderRadius.full'),
          },
        '.btn-green': {
              backgroundColor: theme('colors.green.600'),
              color: theme('colors.white'),
              '&:hover': {
                backgroundColor: theme('colors.transparent'),
                color: theme('colors.green.600')
              },
          },
      }   
      addComponents(buttons)
    }),
  ],
}
