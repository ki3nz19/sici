// Set data slider
import Image from "next/image";

const social = [
    { name: 'Facebook', image: 'https://res.cloudinary.com/standard-insurance/image/upload/v1620359905/sici/Icon_metro-facebook_pgniwj.svg', href: '#', },
    { name: 'Instagram', image: 'https://res.cloudinary.com/standard-insurance/image/upload/v1620359905/sici/Icon_awesome-linkedin_thtec9.svg', href: '#', },
    { name: 'Linkedin', image: 'https://res.cloudinary.com/standard-insurance/image/upload/v1620359905/sici/Icon_awesome-instagram_agpr9u.svg', href: '#', },
    { name: 'Youtube', image: 'https://res.cloudinary.com/standard-insurance/image/upload/v1620359905/sici/ElegantIcons_social_youtube_square_mkaziv.svg', href: '#', }
]

export default function Footer() {
    return (
        <div className="p-10 bg-secondary text-white">
            <div className="lg:flex items-start lg:justify-between">
                <div className="mb-5">
                    <h4 className="uppercase text-sm mb-3">Products and Services</h4>
                    <ul className="text-base list-none font-thin">
                        <li><a href="#">For Individuals</a></li>
                        <li><a href="#">For Businesses</a></li>
                        <li><a href="#">Claims Center</a></li>
                    </ul>
                </div>
                <div className="mb-5">
                    <h4 className="uppercase text-sm mb-3">Our Company</h4>
                    <ul className="text-base list-none font-thin">
                        <li><a href="#">About Standard Insurance</a></li>
                        <li><a href="#">Press</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Digital Affiliates Program</a></li>
                    </ul>
                </div>
                <div className="mb-5">
                    <h4 className="uppercase text-sm mb-3">Connect</h4>
                    <ul className="text-base list-none font-thin">
                        <li><a href="#">(+632) 8845-1111</a></li>
                        <li><a href="#">Online Inquiry</a></li>
                        <li><a href="#">Branches</a></li>
                        <li><a href="#">Branches</a></li>
                        <li><a href="#">Digital Affiliates Program</a></li>
                    </ul>
                </div>
                <div className="mb-5">
                    <h4 className="uppercase text-sm mb-3">Support</h4>
                    <ul className="text-base list-none font-thin">
                        <li><a href="#">Search</a></li>
                        <li><a href="#">Choose Your Topic</a></li>
                        <li><a href="#">Popular Topics</a></li>
                        <li><a href="#">Watch and Learn</a></li>
                    </ul>
                </div>
                <div className="mb-5 md:w-96">
                    <h4 className="uppercase text-sm mb-3">Subcribe to Standard Insurance via Email</h4>
                    <p className="text-sm font-thin">Receive resources & tools that can help you prepare for the future. You can cancel anytime.</p>
                    <form className="sm:flex mt-5">
                        <label htmlFor="emailAddress" className="sr-only">Email address</label>
                        <input id="emailAddress" name="emailAddress" type="email" autoComplete="email" required=""
                           className="w-full border-white bg-transparent px-5 py-3 placeholder-white focus:outline-none focus:border-white focus:ring-offset-white focus:ring-transparent rounded-md text-white text-sm"
                           placeholder="Enter your email" />
                        <button type="submit"
                            className="mt-3 w-full flex items-center justify-center px-5 py-3 border border-transparent text-sm text-orange font-medium rounded-md text-white bg-white hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-cyan-700 focus:ring-green-400 sm:mt-0 sm:ml-3 sm:w-auto sm:flex-shrink-0 uppercase">
                            Subscribe
                        </button>
                    </form>
                </div>
                <div className="mb-5">
                    <div className="flex space-x-4">
                        {social.map((item, key) => (
                            <a key={item.name} href={item.href} className="text-warm-gray-400 hover:text-warm-gray-500">
                                <span className="sr-only">{item.name}</span>
                                <Image className="h-full w-full object-cover object-top" src={item.image} width={30} height={30} />
                            </a>
                        ))}
                    </div>
                </div>
            </div>
            <div className="mt-14 md:max-w-xl md:flex md:items-center justify-between text-white text-sm text-center">
                <p>&#169; 2021 Standard Insurance Co. Inc.</p>
                <a href="#" className="block my-2">Terms of Service</a>
                <a href="#" className="block my-2">Privacy Policy</a>
            </div>
        </div>
    )
}