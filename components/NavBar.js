import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import { MenuIcon, XIcon } from '@heroicons/react/outline'
import Image from 'next/image';

export default function NavBar() {
    // Set navigation
    const navigation = [
        { name: 'Products', href: '#' },
        { name: 'Claims', href: '#' },
        { name: 'Support', href: '#' },
        { name: '(+632) 8845-1111', href: '#' },
    ]

    // Render template
    return (
        <Popover>
            {({ open }) => (
                <div className="bg-white py-6 shadow">
                    <div className="max-w-7xl mx-auto px-4 sm:px-6">
                        <nav
                            className="relative flex items-center justify-between sm:h-10 md:justify-end"
                            aria-label="Global"
                        >
                            <div className="flex items-center flex-1 lg:absolute lg:inset-y-0 lg:left-0">
                                <div className="flex items-center justify-between w-full lg:w-auto">
                                    <a href="#">
                                        <span className="sr-only">Standard Insurance</span>
                                        <Image
                                            className="h-8 w-auto sm:h-10"
                                            src="https://res.cloudinary.com/standard-insurance/image/upload/v1620357876/sici/SICI_logo_3000px_gz6eij.png"
                                            alt="logo"
                                            width={150}
                                            height={40}
                                        />
                                    </a>
                                    <div className="-mr-2 flex items-center lg:hidden">
                                        <Popover.Button className="bg-gray-50 rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-200">
                                            <span className="sr-only">Open main menu</span>
                                            <MenuIcon className="h-6 w-6" aria-hidden="true" />
                                        </Popover.Button>
                                    </div>
                                </div>
                            </div>
                            <div className="hidden lg:flex lg:items-center lg:space-x-10">
                                {navigation.map((item) => (
                                    <a key={item.name} href={item.href} className="uppercase text-lg font-medium text-primary hover:text-gray-400">
                                        {item.name}
                                    </a>
                                ))}
                                <a
                                    href="#"
                                    className="uppercase px-5 py-2 text-lg font-medium text-primary hover:text-white hover:bg-primary border-2 border-solid border-primary rounded-full"
                                >
                                    My Account
                                </a>
                            </div>
                        </nav>
                    </div>

                    <Transition
                        show={open}
                        as={Fragment}
                        enter="duration-150 ease-out"
                        enterFrom="opacity-0 scale-95"
                        enterTo="opacity-100 scale-100"
                        leave="duration-100 ease-in"
                        leaveFrom="opacity-100 scale-100"
                        leaveTo="opacity-0 scale-95"
                    >
                        <Popover.Panel
                            focus
                            static
                            className="absolute z-10 top-0 inset-x-0 p-2 transition transform origin-top-right lg:hidden"
                        >
                            <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
                                <div className="px-5 pt-4 flex items-center justify-between">
                                    <div>
                                        <Image
                                            className="h-8 w-auto"
                                            src="https://res.cloudinary.com/standard-insurance/image/upload/v1620357876/sici/SICI_logo_3000px_gz6eij.png"
                                            alt=""
                                            width={150}
                                            height={40}
                                        />
                                    </div>
                                    <div className="-mr-2">
                                        <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-200">
                                            <span className="sr-only">Close menu</span>
                                            <XIcon className="h-6 w-6" aria-hidden="true" />
                                        </Popover.Button>
                                    </div>
                                </div>
                                <div className="px-2 pt-2 pb-3">
                                    {navigation.map((item) => (
                                        <a
                                            key={item.name}
                                            href={item.href}
                                            className="uppercase block px-3 py-2 rounded-md text-base font-medium text-primary hover:text-gray-400 hover:bg-gray-50"
                                        >
                                            {item.name}
                                        </a>
                                    ))}
                                </div>
                                <a
                                    href="#"
                                    className="uppercase block w-full px-5 py-3 text-center font-medium text-primary hover:text-white hover:bg-primary bg-gray-50"
                                >
                                    My Account
                                </a>
                            </div>
                        </Popover.Panel>
                    </Transition>
                </div>
            )}
        </Popover>
    )
}