export default function Container({children}) {
    return (
        <main className="relative">
            {children}
        </main>
    )
}