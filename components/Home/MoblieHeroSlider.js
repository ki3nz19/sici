import React from 'react';
import Image from 'next/image';
import Carousel from 'react-material-ui-carousel'

export default function MoblieHeroSlider() {

    // Set data slider
    const slider = [
        { image: 'https://res.cloudinary.com/standard-insurance/image/upload/c_scale,q_50,w_360/v1620695041/sici/Hero-Image-1_2x_jxjtsd-Sharpened_kyfgfx.png', paragraph: 'We commit to provide you peace of mind.', },
        { image: 'https://res.cloudinary.com/standard-insurance/image/upload/c_scale,q_50,w_360/v1620697401/sici/Hero_Image_2_2x_krd5wb-Sharpened_aci9yw.png', paragraph: 'We are driven to look for insurance solutions that meet your challenges and needs.', },
        { image: 'https://res.cloudinary.com/standard-insurance/image/upload/c_scale,q_50,w_350/v1620697766/sici/Hero_Image_3_2x_hq6y3o-Sharpened_bgj9vj.png', paragraph: 'We spark confidence that your assets, properties and the lives dear to you are secure.', },
        { image: 'https://res.cloudinary.com/standard-insurance/image/upload/c_scale,q_50,w_360/v1620697978/sici/Hero_Image_4_l6v88a-Sharpened_rwffao.png', paragraph: 'We strive to provide more than what you expect.', }
    ]
    // Render template
    return(
        <div className="carousel relative bg-white">
            <div className="carousel-inner relative overflow-hidden w-full">
                <Carousel
                    interval={8000}
                    indicatorIconButtonProps={{
                        className: 'carousel-indicator',
                        style: {
                            padding: '5px',
                            color: '#7FDDE7',
                        }
                    }}
                    activeIndicatorIconButtonProps={{
                        style: {
                            color: '#278691'
                        }
                    }}
                    indicatorContainerProps={{
                        style: {
                            position: 'absolute',
                            bottom: '15px',
                            textAlign: 'center'
                        }
                    }}
                >
                    {slider.map((item, key) => (
                        <div key={key} className="carousel-item h-96 md:h-lgCarousel">
                            <div className="block h-full w-full relative">
                                <div className="absolute inset-0"> 
                                    <Image src={item.image} layout="fill" objectFit="cover" />
                                </div>
                                <div className="absolute inset-0">
                                    <div className="mx-auto max-w-7xl h-full w-full lg:text-left">
                                        <div className={ key % 2 ? 'h-full flex items-center justify-start' : 'h-full flex items-center justify-end'} >
                                            <h1 className="relative -top-12 text-2xl lg:text-4xl text-gray-900 px-4 w-7/12 md:w-1/2 sm:px-8 xl:pr-16">{item.paragraph}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </Carousel>
            </div>
        </div>
    )
}