import React from 'react';
import Image from 'next/image';
import Link from 'next/link'

const supportLinks = [
    {
      name: 'Jenny Dela Cruz',
      description:
        'Always disappointed in how much insurance will fight against customer in a claim. Just had my first good experience with filing. Unbelievably quick and solid customer experience with Standard Insurance :D',
      imageUrl: "https://res.cloudinary.com/standard-insurance/image/upload/v1620354014/sici/Ellipse_13_2x_bqs3ol.png",
    },
    {
      name: 'Mark Ramos',
      description:
        'WOW - what an amazing company. Applied for house insurance the other day and hands down the best insurance company out there!!!!',
      imageUrl: "https://res.cloudinary.com/standard-insurance/image/upload/v1620360456/sici/Testimonial-Photo-2_xxcxb7.png",
    },
    {
      name: 'Abigail Santos',
      description:
        'I just bought home insurance from standard insurance and I feel so happy and satisfied with their service! Will definitely recommend. :)',
      imageUrl: "https://res.cloudinary.com/standard-insurance/image/upload/v1620359893/sici/Testimonial-Photo-3_mlct70.png",
    },
  ]

const Features = () => {
    return (
        <>
            {/* Features section */}
            <div className="features-container">
                <div className="features-content">
                    <h3 className="features-title">
                        Our Transformative Purpose is to provide Peace of Mind for all Mankind
                    </h3>
                    <p className="features-text-content sm:max-w-prose">
                        We help you adapt to the unexpected,
                        so you can so you can embrace everything life has to offer worry-free.
                        We think ahead and prepare for the worst so you can perform at your best.
                    </p>
                </div>
            </div>
            {/* Testimonial Section */}
            <div className="testimonial-container">
                <div className="testimonial-content">
                    <div className="relative lg:-mt-16 -mb-1">
                        <div aria-hidden="true" className="hidden-absolute-white" />
                        <div className="mx-auto max-w-full px-0 sm:px-6 lg:h-full">
                            <div className="overflow-hidden lg:h-full">
                                <Image
                                    className="testimonial-image"
                                    src="https://res.cloudinary.com/standard-insurance/image/upload/v1620357878/sici/Photos_01_c2rjlp.png"
                                    alt="SICI-img"
                                    width={1200}
                                    height={400}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-orange-300">
                <div className="white-bg-container">
                    <div className="white-bg-content">
                        <p className="white-bg-text">
                            With nothing to hold you back, you’re free to explore new frontiers,
                            welcome exciting opportunities, and plan ahead for the future.
                        </p>
                    </div>
                </div>
                {/* Overlapping cards */}
                <section className="overlap-card-section" aria-labelledby="contact-heading">
                    <h2 className="sr-only" id="contact-heading">Contact us</h2>
                    <div className="overlap-card-container">
                    {supportLinks.map((link) => (
                        <div key={link.name} className="flex flex-col bg-white rounded-2xl shadow-xl pt-4">
                            <div className="grid justify-items-center relative pt-16 px-6 pb-8 md:px-8">
                                <div className="absolute top-0 rounded-full inline-block bg-transparent transform">
                                    <Image
                                        className="h-6 w-6 text-white rounded-full"
                                        src={link.imageUrl}
                                        alt="testimonial-img"
                                        width={50}
                                        height={50}
                                    />
                                </div>
                                <h3 className="text-xl font-medium text-gray-900">{link.name}</h3>
                                <p className="mt-4 text-base text-gray-500">{link.description}</p>
                            </div>
                        </div>
                    ))}
                    </div>
                </section>
                <div className="relative">
                    <div className="relative h-56 sm:h-72 md:absolute md:left-0 md:h-full md:w-1/2">
                        <Image
                            className="w-full h-full object-cover object-top"
                            src="https://res.cloudinary.com/standard-insurance/image/upload/c_scale,q_70,w_700/v1620357888/sici/Girl-Sitting-Photo-01-min_msjsut.webp"
                            alt="Girl-Sitting"
                            layout="fill"
                            objectFit="cover"
                        />
                    </div>
                    <div className="relative mx-auto max-w-md px-4 py-12 sm:max-w-7xl sm:px-6 sm:py-20 md:py-28 lg:px-8 lg:py-32 text-center sm:text-center md:text-left">
                        <div className="md:ml-auto md:w-1/2 md:pl-10">
                            <div>
                                <Image
                                    className="rounded-full"
                                    src="https://res.cloudinary.com/standard-insurance/image/upload/v1620359930/sici/Standard_Prime_Logo_jqe3lx.png"
                                    alt="Standard-prime"
                                    width={67}
                                    height={67}    
                                />        
                            </div>
                            <h4 className="mt-2 text-white text-3xl font-semibold tracking-tight sm:text-4xl">Standard Prime</h4>
                            <p className="mt-3 text-lg text-gray-100">
                                Now, your insurance policy comes with rewards and incentives, discounts and freebies you and your family can enjoy.
                            </p>
                            <div className="mt-8">
                                <div className="inline-flex rounded-full shadow">
                                    <Link href="/" passHref>    
                                        <a className="btn join-btn">JOIN</a>
                                    </Link>        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Features;
