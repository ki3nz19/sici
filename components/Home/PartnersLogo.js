import React from 'react';
import Image from 'next/image';

const PartnersLogo = () => {
    return (
        <div className="bg-white">
            <div className="max-w-7xl mx-auto py-16 px-4 sm:px-6 lg:px-8">
              <p className="text-center text-xl sm:text-4xl font-semibold uppercase text-gray-900 tracking-wide">
                Awards and Recognitions
              </p>
              <div className="mt-6 grid sm:Lgrid-cols-1 gap-8 md:grid-cols-2 lg:grid-cols-3">
                <div className="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
                  <Image className="h-12" src="https://res.cloudinary.com/standard-insurance/image/upload/v1620355399/sici/World_Finance_Logo_lqasrp.png" alt="Tuple" width={150} height={150} />
                </div>
                <div className="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
                  <Image
                    className="h-12"
                    src="https://res.cloudinary.com/standard-insurance/image/upload/v1620355385/sici/GCR_Logo_2x_jtwdwj.png"
                    alt="StaticKit"
                    width={400}
                    height={100}
                  />
                </div>
                <div className="col-span-1 flex justify-center md:col-span-2 md:col-start-2 lg:col-span-1">
                  <Image
                    className="h-12"
                    src="https://res.cloudinary.com/standard-insurance/image/upload/v1620355413/sici/ISO_Logo_bs6htd.png"
                    alt="Transistor"
                    width={150}
                    height={150}
                  />
                </div>
              </div>
            </div>
          </div>
    )
}

export default PartnersLogo;
