import React from 'react';
import Image from 'next/image';
import Link from 'next/link';

const files = [
    {
        title: 'Embracing the new normal in the Philippines',
        news: 'Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps.',
        source:
            'https://res.cloudinary.com/standard-insurance/image/upload/v1620354026/sici/Rectangle_112_2x_mxkm4u.png',
    },
    {
        title: 'Advisory to All Our Property Insurance Customers',
        news: 'Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise.',
        source:
            'https://res.cloudinary.com/standard-insurance/image/upload/v1620354022/sici/Rectangle_111_2x_rxuib1.png',
    },
    {
        title: 'ANC Market Edge - Standard Insurance',
        news: 'Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas.',
        source:
            'https://res.cloudinary.com/standard-insurance/image/upload/v1620354021/sici/Rectangle_110_2x_idax6g.png',
    },
];

const StandardNews = () => {
    return (
        <>
            <div className='news-wrapper-container'>
                <h3 className="news-wrapper-title">Standard Insurance News</h3>
                <ul role="list" className="news-wrapper-content">
                    {files.map((file) => (
                        <li key={file.source} className="relative">
                        <div className="news-images">
                            <Image
                                src={file.source} alt="news-image"
                                className="object-cover pointer-events-none group-hover:opacity-75 w-full"
                                width="700"
                                height="280"
                            />
                            <button type="button" className="absolute inset-0 focus:outline-none">
                            <span className="sr-only">View details for {file.title}</span>
                            </button>
                        </div>
                        <p className="news-content-title">{file.title}</p>
                        <p className="news-text-content">{file.news}</p>
                        </li>
                    ))}
                </ul>
                <div className="mt-8 grid justify-center md:justify-end">
                    <div className="inline-flex rounded-full shadow">
                        <Link href="/" passHref>    
                            <a className="btn join-btn">MORE NEWS</a>
                        </Link>        
                    </div>
                </div>
            </div>
        </>
    )
}

export default StandardNews;
