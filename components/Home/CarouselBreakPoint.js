import { useState, useCallback, useEffect } from 'react';
import HeroSlider from './HeroSlider';
import MoblieHeroSlider from './MoblieHeroSlider';

// @media Query
const useMediaQuery = (width) => {
    const [targetReached, setTargetReached] = useState(false);

    const updateTarget = useCallback((e) => {
        if (e.matches) {
            setTargetReached(true);
        } else {
            setTargetReached(false);
        }
    }, []);

    useEffect(() => {
        const media = window.matchMedia(`(max-width: ${width}px)`)
        media.addEventListener('change', e => updateTarget(e));

        if (media.matches) {
            setTargetReached(true);
        }

        return () => media.removeEventListener(updateTarget);

    }, []);

    return targetReached;
};

const CarouselBreakPoint = () => {

    // Media Query
    const breakPoint = useMediaQuery(640);

    return (
        <>
            {breakPoint ? (
                <MoblieHeroSlider />
                ) : (
                <HeroSlider />   
            )}
        </>
    )
}

export default CarouselBreakPoint