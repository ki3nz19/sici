import React from "react";
import Image from "next/image";
import Link from "next/link"

const product = [
  {
    name: "Car Protect",
    title: "Safe drives make the road all the more enjoyable.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354015/sici/Car_bgvyzt.svg",
  },
  {
    name: "Motorcycle Protect",
    title: "Make the most out of every ride.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354016/sici/Motorcycle_bc4kkv.svg",
  },
  {
    name: "CTPL Cover",
    title: "Comply with legal regulations that protect against liability for death or bodily injury.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354014/sici/CTPL_dqsroo.svg",
  },
  {
    name: "Travel Protect",
    title: "Enjoy every adventure, worry-free.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354021/sici/Travel_bbcn5c.svg",
  },
  {
    name: "House Protect",
    title: "Your dream home should be a safe haven.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354015/sici/House_t5ovon.svg",
  },
  {
    name: "Cellphone Protect",
    title: "Get coverage for the gadget you rely on the most.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354016/sici/Path_1053_fdpjo5.svg",
  },
  {
    name: "Bayaning Guro Protect",
    title: "Teachers are our heroes, so we do our best to protect them in our own way.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354015/sici/Guro_vj4c71.svg",
  },
  {
    name: "Kasambahay Protect",
    title: "Our ates and kuyas love us like family, so let’s protect them like they’re ours.",
    role: "LEARN MORE",
    imageUrl:
      "https://res.cloudinary.com/standard-insurance/image/upload/v1620354016/sici/kasambahay_myg83y.svg",
  },
];

const Product = () => {
    return (
      <>
        <div className="px-10">
            <ul className="product-wrapper">
                {product.map((item) => (
                <li
                    key={item.name}
                    className="product-container"
                >
                    <div className="product-content">
                    <Image
                        className="product-img"
                        src={item.imageUrl}
                        alt="Product-image"
                        width={100}
                        height={100}
                    />
                    <h3 className="product-name">
                        {item.name}
                    </h3>
                    <div className="product-title">
                        <span className="sr-only">Title</span>
                        <p className="text-gray-500 text-sm">{item.title}</p>
                        <span className="sr-only">Role</span>
                            <div className="mt-6">
                                <Link href='/' passHref>      
                                    <a className="btn btn-green">{item.role}</a>
                                </Link> 
                            </div>
                    </div>
                    </div>
                </li>
                ))}
            </ul>
            </div>
        </>
  );
};

export default Product;
