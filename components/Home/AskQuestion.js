import React from 'react';
import Image from "next/image";
import Link from "next/link"
import { Disclosure } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/outline'

const faqs = [
    {
        question: "Am I qualified to file a Claim? Who is qualified?",
        answer:
            "You may Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    },
    {
        question: "I want to file a claim. What are the requirements?",
        answer:
            "",
    },
    {
        question: "When should I file the Claim?",
        answer:
            "",
    },
    {
        question: "Can a representative transact on my behalf?",
        answer:
            "",
    },
    {
        question: "What kind of insurance is best for me?",
        answer:
            "",
    },
    
];
  
function classNames(...classes) {
return classes.filter(Boolean).join(' ')
}

const product = [
    {
      name: "Be our Partner",
      title: "Life as a Standard Insurance Advisor is as rewarding as it can get! Explore the world of a non-life insurance advisor at Standard Insurance.",
      role: "LEARN MORE",
      imageUrl:
        "https://res.cloudinary.com/standard-insurance/image/upload/v1620354014/sici/Be_our_Partner_bh8q8f.svg",
    },
    {
      name: "Talk to Us",
      title: "Whatever challenges you’re facing, we’re here to listen, learn, and adapt. Because that’s what brings your insurance policy to life.",
      role: "LEARN MORE",
      imageUrl:
        "https://res.cloudinary.com/standard-insurance/image/upload/v1620396889/sici/Talk_To_Us_sxhte2.svg",
    },  
  ];

const AskQuestion = () => {
    return (
        <>
            <div className="px-10 py-4">
                <ul className="question-wrapper">
                    {product.map((item) => (
                    <li key={item.name} className="product-container">
                        <div className="product-content">
                            <Image
                                className="product-img"
                                src={item.imageUrl}
                                alt="BeOurPartner-image"
                                width={100}
                                height={120}
                            />
                            <h3 className="product-name">{item.name}</h3>
                            <div className="product-title">
                                <span className="sr-only">Title</span>
                                <p className="text-gray-500 text-sm">{item.title}</p>
                                <span className="sr-only">Role</span>
                                <div className="mt-6">
                                    <Link href='/' passHref>      
                                        <a className="btn btn-green">{item.role}</a>
                                    </Link> 
                                </div>
                            </div>
                        </div>
                    </li>
                ))}
                </ul>
                <div className='py-10'>       
                    <div className="bg-white">
                        <div className="aq-wrapper-container">
                            <div className="aq-wrapper-content">
                                <h3 className="aq-title">Frequently Asked Questions</h3>
                                <dl className="aq-container">
                                    {faqs.map((faq) => (
                                    <Disclosure as="div" key={faq.question} className="pt-6">
                                        {({ open }) => (
                                        <>
                                            <dt className="text-base sm:text-lg">
                                            <Disclosure.Button className="disclosure-button">
                                                <span className="font-medium text-gray-900">{faq.question}</span>
                                                <span className="ml-6 h-7 flex items-center">
                                                <ChevronDownIcon
                                                    className={classNames(open ? '-rotate-180' : 'rotate-0', 'h-6 w-6 transform')}
                                                    aria-hidden="true"
                                                />
                                                </span>
                                            </Disclosure.Button>
                                            </dt>
                                            <Disclosure.Panel as="dd" className="mt-2 pr-12">
                                            <p className="text-base sm:text-lg text-gray-500">{faq.answer}</p>
                                            </Disclosure.Panel>
                                        </>
                                        )}
                                    </Disclosure>
                                    ))}
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AskQuestion;
