const optionsGetAQuote = [
    { name: 'Car Insurance', href: '#' },
    { name: 'CTPL Cover', href: '#' },
    { name: 'Motorcycle Insurance', href: '#' },
    { name: 'House Insurance', href: '#' },
    { name: 'Travel Insurance', href: '#' },
    { name: 'Cellphone Insurance', href: '#' },
    { name: 'Bayaning Guro Protect', href: '#' },
    { name: 'Kasambahay Protect', href: '#' },
]

const optionsPayPremiumOnline = [
    { name: 'Credit Card', href: '#' },
    { name: 'Online Banking', href: '#' },
    { name: 'GCash', href: '#' },
    { name: 'Bayad Center', href: '#' },
]

export default function GetAQuote() {
    return (
        <div className="p-10 bg-secondary">
            <div className="max-w-7xl mx-auto px-4 sm:px-6">
                <div className="grid md:grid-cols-3 gap-6 text-center">
                    <div>
                        <h2 className="gaq-title">Get A Quote</h2>
                        <select
                            className="gaq-select"
                        >
                            <option>GET A QUOTE</option>
                            {optionsGetAQuote.map((item) => (
                                <option key={item.name} value={item.href}>{item.name}</option>
                            ))}
                        </select>
                        <a href="#" className="gaq-button">Retrieve a Quote</a>
                    </div>
                    <div>
                        <h2 className="gaq-title">File a Claim</h2>
                        <a href="#" className="gaq-button">New Claim</a>
                        <a href="#" className="gaq-button">Exiting Claim</a>
                    </div>
                    <div>
                        <h2 className="gaq-title">Pay Premiums Online</h2>
                        <select
                            className="gaq-select"
                        >
                            <option>SELECT</option>
                            {optionsPayPremiumOnline.map((item) => (
                                <option key={item.name} value={item.href}>{item.name}</option>
                            ))}
                        </select>
                        <a href="#" className="gaq-button">Submit</a>
                    </div>
                </div>
            </div>
        </div>
    )
}