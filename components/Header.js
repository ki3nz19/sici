import Head from "next/head";
import { Fragment } from 'react';

export default function Header(props) {
    return (
        <Fragment>
            <Head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />

                <title>{props.title}</title>
                <link rel="icon" href="/favicon.ico" />
                
                <meta name="title" content={process.env.META_TITLE} />
                <meta name="description" content={process.env.META_DESCRIPTION} />

                <meta property="og:type" content="website" />
                <meta property="og:url" content={process.env.BASE_URL} />
                <meta property="og:title" content={process.env.META_TITLE} />
                <meta property="og:description" content={process.env.META_DESCRIPTION} />
                <meta property="og:image" content={process.env.META_IMAGE} />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content={process.env.BASE_URL} />
                <meta property="twitter:title" content={process.env.META_TITLE} />
                <meta property="twitter:description" content={process.env.META_DESCRIPTION} />
                <meta property="twitter:image" content={process.env.META_IMAGE} />

                <link rel="preload" as="image" href="https://res.cloudinary.com/standard-insurance/image/upload/v1620645537/sici/Hero_Image_1_natgcq.png" />
                <link rel="preload" as="image" href="https://res.cloudinary.com/standard-insurance/image/upload/q_70/v1620353831/sici/Hero_Image_2_jb92yc.png" />
                <link rel="preload" as="image" href="https://res.cloudinary.com/standard-insurance/image/upload/q_70/v1620359680/sici/Hero_Image_3_gyr8lx.png" />
                <link rel="preload" as="image" href="https://res.cloudinary.com/standard-insurance/image/upload/q_70/v1620360074/sici/Hero_Image_4_l6v88a.png" />
            </Head>
            
        </Fragment>
    )
}